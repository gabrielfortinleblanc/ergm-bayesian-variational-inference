"""
This module is used by pytest to test the module dataset.
"""
import os
from ergm.dataset import get_karate_graph, get_teen_graphs


def test_get_karate_graph():
    data_dir = os.path.join('data', 'karate')
    filename = 'out.ucidata-zachary'
    G = get_karate_graph(data_dir, filename)
    assert G.number_of_nodes() == 34
    assert G.number_of_edges() == 78


def test_get_teen_graphs():
    data_dir = os.path.join('data', 'teen')
    filenames = ['s50-network1.dat', 's50-network2.dat', 's50-network3.dat',
                 's50-alcohol.dat', 's50-smoke.dat', 's50-sport.dat',
                 's50-drugs.dat']
    Gs = get_teen_graphs(data_dir, filenames)
    assert len(Gs) == 3
    for i in range(3):
        assert Gs[i].number_of_nodes() == 50
        for _, u in Gs[i].nodes(data=True):
            assert u['drink'] in {1, 2, 3, 4, 5}
            assert u['smoke'] in {1, 2, 3}
            assert u['sport'] in {1, 2}
            assert u['drugs'] in {1, 2, 3, 4}
