from ergm.model import Model
from ergm.sampling import metropolis_hastings, parallel_metropolis_hastings
import numpy as np
import networkx as nx


def test_metropolis_hastings():
    # Basic tests are used since it is totally random. Take a look to the
    # notebooks for more details.
    G = nx.complete_graph(5)
    model = Model([nx.Graph.number_of_edges], [None])
    sample = metropolis_hastings(1000, G, np.array([1]), model, 1000)
    assert len(sample) == 1000
    sample = metropolis_hastings(10, G, np.array([1]), model, 1000, 10)
    assert len(sample) == 10
    sample = metropolis_hastings(1, G, np.array([-1]), model, 1)
    assert len(sample) == 1


def test_parallel_metropolis_hastings():
    # Basic tests are used since it is totally random. Take a look to the
    # notebooks for more details.
    G = nx.complete_graph(5)
    model = Model([nx.Graph.number_of_edges], [None])
    sample = parallel_metropolis_hastings(1000, G, np.array([1]), model, 1000)
    assert len(sample) == 1000
    sample = parallel_metropolis_hastings(10, G, np.array([1]), model, 1000,
                                          10)
    assert len(sample) == 10
    sample = parallel_metropolis_hastings(1, G, np.array([-1]), model, 1,
                                          nworkers=2)
    assert len(sample) == 1
