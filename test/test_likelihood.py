import networkx as nx
from ergm.model import Model
from ergm.model import sufficient
from ergm import likelihood


def test_pseudolikelihood_estimation():
    # It is hard to test this function, so the test are quite basics.
    binary_tree = nx.balanced_tree(2, 4)
    model = Model(
        [
            sufficient.geo_weighted_degree,
            sufficient.geo_weighted_ew_shared_partners,
        ],
        [
            [0.2],
            [0.2],
        ]
    )
    param = likelihood.maximum_pseudolikelihood_estimator(binary_tree, model)
    assert param.shape == (2,)

    model = Model(
        [
            nx.Graph.number_of_edges,
        ],
        [
            None,
        ]
    )
    param = likelihood.maximum_pseudolikelihood_estimator(binary_tree, model)
    assert param.shape == ()


def test_maximum_likelihood_estimation():
    sample = nx.balanced_tree(2, 4)
    model = Model(
        [
            sufficient.geo_weighted_degree,
            sufficient.geo_weighted_ew_shared_partners,
        ],
        [
            [0.2],
            [0.2],
        ]
    )
    param0 = likelihood.maximum_pseudolikelihood_estimator(sample, model)
    param = likelihood.maximum_likelihood_estimator(sample, model, param0)
    assert param.shape == (2,)
