from ergm.model.sufficient import \
        geo_weighted_degree, \
        geo_weighted_ew_shared_partners
import networkx as nx
import pytest
import numpy as np


def test_geo_weighted_degree():
    binary_tree = nx.balanced_tree(2, 4)
    assert pytest.approx(geo_weighted_degree(binary_tree, 0.2)) == \
        np.exp(0.2) * ((1 - (1 - np.exp(-0.2)) ** 1) * 16 +
                       (1 - (1 - np.exp(-0.2)) ** 2) * 1 +
                       (1 - (1 - np.exp(-0.2)) ** 3) * 14)
    assert pytest.approx(geo_weighted_degree(binary_tree, 0.8)) == \
        np.exp(0.8) * ((1 - (1 - np.exp(-0.8)) ** 1) * 16 +
                       (1 - (1 - np.exp(-0.8)) ** 2) * 1 +
                       (1 - (1 - np.exp(-0.8)) ** 3) * 14)
    complete_graph = nx.complete_graph(4)
    assert pytest.approx(geo_weighted_degree(complete_graph, 0.3)) == \
        np.exp(0.3) * (1 - (1 - np.exp(-0.3)) ** 3) * 4
    assert pytest.approx(geo_weighted_degree(complete_graph, 0.7)) == \
        np.exp(0.7) * (1 - (1 - np.exp(-0.7)) ** 3) * 4


def test_geo_weighted_ew_shared_partners():
    binary_tree = nx.balanced_tree(2, 4)
    assert pytest.approx(
            geo_weighted_ew_shared_partners(binary_tree, 0.2)) == 0

    complete_graph = nx.complete_graph(4)
    assert pytest.approx(
        geo_weighted_ew_shared_partners(complete_graph, 0.1)
        ) == np.exp(0.1) * (1 - (1 - np.exp(-0.1)) ** 2) * 6
