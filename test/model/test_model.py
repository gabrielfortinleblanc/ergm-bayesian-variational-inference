import numpy as np
import networkx as nx
from ergm.model import Model
from ergm.model import sufficient


def test_model():
    binary_tree = nx.balanced_tree(2, 4)
    model = Model(
        [
            sufficient.geo_weighted_degree,
            sufficient.geo_weighted_ew_shared_partners,
        ],
        [
            [0.2],
            [0.2],
        ]
    )
    sufficient_vector = model.sufficient(binary_tree)
    assert np.isclose(sufficient_vector, np.array([
        np.exp(0.2) * ((1 - (1 - np.exp(-0.2)) ** 1) * 16 +
                       (1 - (1 - np.exp(-0.2)) ** 2) * 1 +
                       (1 - (1 - np.exp(-0.2)) ** 3) * 14),
        0])).all()
