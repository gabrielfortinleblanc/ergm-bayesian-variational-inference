import networkx as nx
import numpy as np
from ergm.model import Model
from ergm.model import sufficient
from ergm import sampling
from ergm import mle


def test_pseudolikelihood_estimation():
    # It is hard to test this function, so the test are quite basics.
    binary_tree = nx.balanced_tree(2, 4)
    model = Model(
        [
            sufficient.geo_weighted_degree,
            sufficient.geo_weighted_ew_shared_partners,
        ],
        [
            [0.2],
            [0.2],
        ]
    )
    param = mle.pseudolikelihood_estimation(binary_tree, model)
    assert param.shape == (2,)

    model = Model(
        [
            nx.Graph.number_of_edges,
        ],
        [
            None,
        ]
    )
    param = mle.pseudolikelihood_estimation(binary_tree, model)
    assert param.shape == ()


def test_maximum_likelihood_estimation():
    sample = nx.balanced_tree(2, 4)
    model = Model(
        [
            sufficient.geo_weighted_degree,
            sufficient.geo_weighted_ew_shared_partners,
        ],
        [
            [0.2],
            [0.2],
        ]
    )
    param0 = mle.pseudolikelihood_estimation(sample, model)
    param = mle.maximum_likelihood_estimator(sample, model, param0)
    assert param.shape == (2,)
