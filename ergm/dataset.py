"""
This module contains functions used to load the data set as a NetworkX graph.
"""
import os

import networkx as nx
import numpy as np


def get_karate_graph(data_dir, filename):
    """
    Loads the karate network and returns it as NetworkX graph. This dataset
    contains only one network. For more details, refer to the README associated
    to it.

    Paramters
    =========
    data_dir : str
        The data directory.
    filename : str
        The filename that must be in the data directory.

    Returns
    =======
    A NetworkX graph representing the karate club network.
    """
    file_path = os.path.join(data_dir, filename)
    G = nx.read_edgelist(file_path, comments='%')
    return G


def get_teen_graphs(data_dir, filenames):
    """
    Loads the teenage friends networks and returns them as NetworkX graph.
    This dataset contains the three networks representing friendships among
    50 teenagers. There is also information on each teenager about different
    behaviors. For more details, refer to the HTM file associated to it.

    Parameters
    ==========
    data_dir : str
        The data directory.
    filenames : list of str
        A iterable of seven filenames is this order:
            1. the three friendships networks,
            2. drink behaviors,
            3. smoke behaviors,
            4. sport behaviors and
            5. drugs behaviors.

    Returns
    =======
    A list of three NetworkX graph where each nodes contains the information
    about the teenager under `drink`, `smoke`, `sport` and `drugs` atttributes.
    """
    drink = np.loadtxt(os.path.join(data_dir, filenames[3]))
    smoke = np.loadtxt(os.path.join(data_dir, filenames[4]))
    sport = np.loadtxt(os.path.join(data_dir, filenames[5]))
    drugs = np.loadtxt(os.path.join(data_dir, filenames[6]))

    graphs = list()
    for t in range(3):  # Loop over the three networks
        adj_matrix = np.loadtxt(os.path.join(data_dir, filenames[t]))
        G = nx.from_numpy_array(adj_matrix)

        attributes = {
                n: {'drink': drink[n, t],
                    'smoke': smoke[n, t],
                    'sport': sport[n, t],
                    'drugs': drugs[n, t]}
                for n in range(50)  # For each nodes
        }
        nx.set_node_attributes(G, attributes)

        graphs.append(G)
    return graphs
