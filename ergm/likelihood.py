"""
This module contains functions that involves the likelihood.
"""
import itertools
import logging

import numpy as np
import scipy
from scipy import optimize, spatial
from scipy.spatial._qhull import QhullError
from sklearn import linear_model

from . import sampling


def maximum_likelihood_estimator(obs, model, param0,
                                 nsamples0=100, burnin=1000, thinning=100,
                                 bound=1e-1, gamma_tol=1e-3, keep_trace=False,
                                 max_iter=100):
    """
    Estimates the maximum likelihood estimator using a two-phase algorithm.
    More details are in the README file.

    Parameters
    ==========
    obs : NetworkX Graph
        The observed graph.
    model : Model
        The model used in the analysis.
    param0 : Numpy Array
        The vector of the constant parameters. It has to be close to the seeked
        value.
    nsamples0 : int
        The number of sample to generate to approximate the expected value of
        the sufficient statistics. The double of this value will be used at the
        final iteration. By default, 100 is used.
    burnin : int
        The burnin used when Monte Carlo Markov chain is used to sample. By
        default, 1000 is used.
    thinning : int
        The thinning factor used when Monte Carlo Markov chain is used to
        sample. By default, 100 is used.
    bound : float
        A bound when optimizing the log ratio of likelihoods. It avoids great
        steps of the parameter between two iterations. The final iteration is
        not bounded.By default, it is at 0.1.
    gamma_tol : float
        The gamma parameter is used to choose the pseudo-observation.
        We consider that our observation is equal to the pseudo-observation if
        the distance between both is less than `gamma_tol`.
    keep_trace : bool
        If it's at True, then multiple informations are kept through the
        iterations and returned with the result. At each iteration, a 4-tuple
        contains the information in this order:
        1. The parameter at the iteration.
        2. The gamma used to computes the pseudo osbservation.
        3. A Numpy Array with the sufficient statistics of each generated graph
           as row.
        4. A Scipy OptimizeResult object, which other information about the
           optimization phase.
        By default, this flag is at False.
    max_iter : int
        The maximum number of iteration. If `max_iter` is reached, then
        the algorithm is considered to not have converged. By default, max_iter
        is at 100.
    Returns
    =======
    An estimation of the maximum likelihoods estimator. If `keep_trace` is at
    True, then a 2-tuple is returned, where the first element is the estimation
    and the second element is a list of 4-tuple. If the algorithm did not
    converge, then None replaces the estimation.
    """
    logger = logging.getLogger(__name__)
    logger.info('The function maximum_likelihood_estimator is called.')

    obs_ss = model.sufficient(obs)
    trace = list() if keep_trace else None
    param = param0
    times_inside = 0
    it = 0

    logger.info('Start iterating')
    while times_inside < 2:
        # Sample
        logger.info('Parameter: {}'.format(param))

        samples0_ss = samples0_ss_mean = delaunay = None
        factor = 1
        while delaunay is None:
            samples0 = sampling.metropolis_hastings(nsamples0, obs, param,
                                                    model, burnin, thinning)
            samples0_ss = np.array([model.sufficient(g) for g in samples0])
            samples0_ss_mean = samples0_ss.mean(0)
            try:
                delaunay = spatial.Delaunay(samples0_ss)
            except QhullError:
                if factor == 8:
                    logger.info('The algorithm did not converge. '
                                'The sufficient statistics of the generated '
                                'graphs are always affinely dependent.')
                    return (None, trace) if keep_trace else None
                factor *= 2

        # Choose the pseudo-observation
        pseudo_sample_ss = None
        # Find greatest gamma which we have the pseudo-observation in the
        # convex hull.
        inf_gamma = 0
        sup_gamma = 1
        while sup_gamma - inf_gamma > gamma_tol:
            mid_gamma = (sup_gamma + inf_gamma) / 2
            pseudo_sample_ss = mid_gamma * obs_ss + \
                (1 - mid_gamma) * samples0_ss_mean
            if delaunay.find_simplex(pseudo_sample_ss) >= 0:
                inf_gamma = mid_gamma
            else:
                sup_gamma = mid_gamma
        gamma = (sup_gamma + inf_gamma) / 2
        pseudo_sample_ss = gamma * obs_ss + (1 - gamma) * samples0_ss_mean

        result = optimize.minimize(
            lambda t, t0, s, s0: -_log_ratio_likelihoods(t, t0, s, s0), param,
            jac=lambda t, t0, s, s0:
                -_gradient_log_ratio_likelihoods(t, t0, s, s0),
            args=(param, pseudo_sample_ss, samples0_ss),
            bounds=zip(param - bound, param + bound)
        )
        if keep_trace:
            trace.append((param, gamma, samples0_ss, result))
        if not result.success:
            logger.info('The algorithm did not converge.')
            return (None, trace) if keep_trace else None
        param = result.x

        if gamma > 1 - gamma_tol:
            times_inside += 1
        else:
            times_inside = 0

        it += 1
        if it == max_iter:
            logger.info('The algorithm did not converge by reaching the '
                        'maximum number of iterations.')
            return (None, trace) if keep_trace else None

    logger.info('Parameter: {}'.format(param))
    logger.info('Final iteration')
    # Final iteration with twice sample0 size
    samples0 = sampling.metropolis_hastings(2 * nsamples0, obs, param, model,
                                            burnin, thinning)
    samples0_ss = np.array([model.sufficient(g) for g in samples0])
    samples0_ss_mean = samples0_ss.mean(0)

    result = optimize.minimize(
        lambda t, t0, s, s0: -_log_ratio_likelihoods(t, t0, s, s0), param,
        jac=lambda t, t0, s, s0:
            -_gradient_log_ratio_likelihoods(t, t0, s, s0),
        args=(param, pseudo_sample_ss, samples0_ss),
    )
    if keep_trace:
        trace.append((param, 1, samples0_ss, result))
    if not result.success:
        logger.info('The algorithm did not converge.')
        return (None, trace) if keep_trace else None

    return (result.x, trace) if keep_trace else result.x


def maximum_pseudolikelihood_estimator(obs, model):
    """
    Approximates the maximum likelihood estimator using the pseudolikelihood
    function with respect to the observed graph and the model used.

    Parameters
    ==========
    obs : NetworkX Graph
        The observed graph.
    model : Model
        The model used.
    Returns
    =======
    The parameter as Numpy array that best fit the pseudolikelihood and the
    observation.
    """
    logger = logging.getLogger(__name__)
    logger.info('The function maximum_pseudolikelihood_estimator is called.')

    # Create the "data set"
    X = list()
    y = list()
    sufficient0 = sufficient1 = None
    for u, v in itertools.combinations(obs.nodes(), 2):
        if obs.has_edge(u, v):  # Bit at 1
            sufficient1 = model.sufficient(obs)
            obs.remove_edge(u, v)
            sufficient0 = model.sufficient(obs)
            obs.add_edge(u, v)
            y.append(1)
        else:  # Bit at 0
            sufficient0 = model.sufficient(obs)
            obs.add_edge(u, v)
            sufficient1 = model.sufficient(obs)
            obs.remove_edge(u, v)
            y.append(0)
        X.append(sufficient1 - sufficient0)
    X = np.array(X)
    y = np.array(y)

    # Fit and return the parameter
    lr = linear_model.LogisticRegression(penalty='none')
    lr.fit(X, y)
    return np.squeeze(lr.coef_)


def pseudolikelihood(graph, model, param):
    """
    Computes the pseudolikelihood density function with respect to the input.

    Parameters
    ==========
    graph : NetworkX Graph
        The graph to evaluate the density function.
    model : Model
        The model used.
    param : Numpy Array
        The parameter used with the model.
    Returns
    =======
    The pseudolikelihood density evaluated with respect to the input.
    """
    logger = logging.getLogger(__name__)
    logger.info('The function pseudolikelihood is called.')

    change_statistics = list()
    y = list()
    sufficient0 = sufficient1 = None
    for u, v in itertools.combinations(graph.nodes(), 2):
        if graph.has_edge(u, v):  # Bit at 1
            sufficient1 = model.sufficient(graph)
            graph.remove_edge(u, v)
            sufficient0 = model.sufficient(graph)
            graph.add_edge(u, v)
            y.append(1)
        else:  # Bit at 0
            sufficient0 = model.sufficient(graph)
            graph.add_edge(u, v)
            sufficient1 = model.sufficient(graph)
            graph.remove_edge(u, v)
            y.append(0)
        change_statistics.append(sufficient1 - sufficient0)
    change_statistics = np.array(change_statistics)
    y = np.array(y)

    return np.exp(
        (
            y * change_statistics @ param
            - np.log(1 + np.exp(change_statistics @ param))
        ).sum()
    )


def adjusted_pseudolikelihood(graph, model,
                              param, param_mle, param_mple,
                              mle_normalizing_constant=None,
                              nsamples=200, burnin=1000,
                              thinning=100):
    """
    Computes the adjusted pseudolikelihood density function with respect to the
    input.

    Parameters
    ==========
    graph : NetworkX Graph
        The graph to evaluate the density function.
    model : Model
        The model used.
    param : Numpy Array
        The parameter used with the model.
    param_mle : Numpy Array
        The maximum likelihood estimator.
    param_mple : Numpy Array
        The maximum pseudolikelihood estimator.
    mle_normalizing_constant : float
        An estimation of the normalizing constant of the likelihood when the
        parameter is the MLE. If it is not informed, then it will be computed.
    nsamples : int
        The number of samples to use when sampling. By default, it is at 200.
    burnin : int
        The burn-in to use when sampling. By default, it is at 1000.
    thinning : int
        The thinning factor to use when sampling. By default, it is at 100.
    Returns
    =======
    The adjusted pseudolikelihood density evaluated with respect to the input.
    """
    graph_ss = model.sufficient(graph)

    if mle_normalizing_constant is None:
        pass  # Compute it
    samples_mple = sampling.metropolis_hastings(nsamples, graph, param_mple,
                                                model, burnin, thinning)
    samples_mple_ss = np.array([model.sufficient(g) for g in samples_mple])
    mple_ss_cov_chol = scipy.linalg.cholesky(-np.cov(samples_mple_ss,
                                                     rowvar=False))
    samples_mle = sampling.metropolis_hastings(nsamples, graph, param_mle,
                                               model, burnin, thinning)
    samples_mle_ss = np.array([model.sufficient(g) for g in samples_mle])
    mle_ss_cov_chol = scipy.linalg.cholesky(-np.cov(samples_mle_ss,
                                                    rowvar=True))

    # Compute the pseudolikelihood for param
    pl = pseudolikelihood(graph, model, param)

    curvature_correction = np.linalg.inv(mple_ss_cov_chol) @ mle_ss_cov_chol
    mode_correction = param_mple + curvature_correction * (param - param_mle)
    magnitude_correction = np.exp(param_mle @ graph_ss) \
        / mle_normalizing_constant / pl

    return magnitude_correction * pseudolikelihood(graph, mode_correction)


def _log_ratio_likelihoods(param, param0, sample_ss, sample0_ss):
    """
    Computes the log ratio likelihoods with respect to the parameter `param`.

    Parameters
    ==========
    param : Numpy Array
        The vector of the parameters.
    param0 : Numpy Array
        The vector of the constant parameters. It has to be close to the seeked
        value.
    sample_ss : Numpy Array
        A numpy array of the sufficient statistics of the observed graph.
    sample0 : Numpy Array
        A numpy array of the sufficient statistics of generated graphs with
        respect to `param0`.
    Returns
    =======
    The log ratio of likelihoods with respect to the input.
    """
    s = sample0_ss @ (param - param0)
    s_max = s.max(0)
    return sample_ss @ (param - param0) - s_max - \
        np.log(np.exp(s - s_max).mean())


def _gradient_log_ratio_likelihoods(param, param0, sample_ss, sample0_ss):
    """
    Computes the gradient of the log ratio likelihoods with respect to the
    parameter `param`.

    Parameters
    ==========
    param : Numpy Array
        The vector of the parameters.
    param0 : Numpy Array
        The vector of the constant parameters. It has to be close to the seeked
        value.
    sample_ss : Numpy Array
        A numpy array of the sufficient statistics of the observed graph.
    sample0 : Numpy Array
        A numpy array of the sufficient statistics of generated graphs with
        respect to `param0`.
    Returns
    =======
    The gradient with respect to the input.
    """
    s = sample0_ss @ (param - param0)
    s_max = s.max(0)
    w = np.exp(s - s_max)
    return (w[:, np.newaxis] * (sample_ss - sample0_ss) / w.sum()).sum(0)
