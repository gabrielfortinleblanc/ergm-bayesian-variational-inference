import numpy as np
from scipy.spatial.distance import cdist
import scipy
import logging
from . import sampling
from . import mle
from . import normalizing_constant_estimator


def _vech(a):
    # A function used to vectorized lower triangular matrix.
    return a[np.tril_indices(a.shape[0])]


def elbo_estimator(param, chol, normal_sample,
                   sample_ss, sample_mle_ss, param_mle,
                   log_normalizing_constant, log_det_var_init, inv_var_init,
                   mean_init):
    """
    It computes an estimator of the evidence lower bound (ELBO) that is needed
    when executing the SVI algorithm proposed by Linda S.L. Tan & Nial Friel in
    "Bayesian Variational Inference for Exponential Random Graph Models".

    Parameters
    ==========
    param : Numpy Array
        The parameter of the distribution.
    chol : Numpy Array
        The lower triangular matrix which is the resulting of the Cholesky
        decomposition of the covariance matrix.
    normal_sample : Numpy Array
        A sample of a multivariate normal distribution.
    sample_ss : Numpy Array
        The sufficient statistics of the observed graph.
    sample_mle_ss : Numpy Array
        The sufficient statistics of samples simulated with respect to the MLE.
    param_mle : Numpy Array
        The maximum likelihood estimator.
    log_normalizing_constant : Numpy Array
        An estimator of the normalizing constant of the distribution when the
        parameter if the MLE.
    log_det_var_init : Numpy Array
        The logarithm of the determinant of the initial covariance matrix.
    inv_var_init : Numpy Array
        The inverse of the initial covariance matrix.
    mean_init : Numpy Array
        The initial mean.
    Returns
    =======
    The estimator of the ELBO.
    References
    ==========
    Linda S. L. Tan & Nial Friel (2020) Bayesian Variational Inference for
    Exponential Random Graph Models, Journal of Computational and Graphical
    Statistics, 29:4, 910-928, DOI: 10.1080/10618600.2020.174.0714
    """
    logger = logging.getLogger(__name__)
    # logger.info('The function elbo_estimator is called.')

    z = sample_mle_ss @ (param - param_mle)
    z_max = z.max()
    normalized_param = param - mean_init
    return param @ sample_ss - log_normalizing_constant \
        - z_max - np.log(np.exp(z - z_max).mean()) \
        - 0.5 * log_det_var_init \
        - 0.5 * normalized_param @ inv_var_init @ normalized_param \
        + np.log(np.linalg.det(chol)) + 0.5 * normal_sample @ normal_sample


def svi_normal_posterior_approximation(obs, model, mu_init, var_init,
                                       nsamples, burnin, thinning,
                                       param_mle=None, tol=1e-5,
                                       learning_step=0.001,
                                       first_moment_decay=0.9,
                                       second_moment_decay=0.999,
                                       method='snis'):
    """
    Fit a normal distribution to best approximate the posterior distribution of
    the parameter. It uses adaptative moment estimation (Adam) to maximize an
    estimation of the evidence lower bound (ELBO).

    obs : NetworkX Graph
        The observed graph.
    model : Model
        The model used.
    mu_init : Numpy Array
        The initial mean of the normal posterior approximation.
    var_init : Numpy Array
        The initial variance of the normal posterior approximation.
    nsamples : int
        The size of generated samples for approximating the expected value of
        the sufficient statistics.
    burnin : int
        The burn-in used for generating samples.
    thinning : int
        The thinning factor used for generating samples.
    param_mle : Numpy Array
        The maximum likelihood estimator (MLE). If it is at None, then the MLE
        will be computed using the `mle` module. If the algorithm in `mle`
        fails to find the MLE, then None will be returned. By default, it is at
        None.
    tol : float
        The tolerance used to stop the iterations. By default, the value is
        1e-5.
    method : str
        The method used for estimating the expected value of the sufficient
        statistics. It could be either by self-normalized importance sampling
        (snis) or by Monte Carlo sampling (montecarlo). By default, it is at
        'snis'.
    Returns
    =======
    A 2-tuple where the first element is the mean and the second the variance.
    References
    ==========
    Linda S. L. Tan & Nial Friel (2020) Bayesian Variational Inference for
    Exponential Random Graph Models, Journal of Computational and Graphical
    Statistics, 29:4, 910-928, DOI: 10.1080/10618600.2020.174.0714
    """
    logger = logging.getLogger(__name__)
    logger.info('The function svi_normal_posterior_approximation is called.')

    logger.info('Compute the sufficient statistics of the observation.')
    obs_ss = model.sufficient(obs)

    if param_mle is None:
        logger.info('Compute the maximum likelihood estimator.')
        param_mple = mle.pseudolikelihood_estimation(obs, model)
        param_mle = mle.maximum_likelihood_estimator(obs, model, param_mple)
        if param_mle is None:
            return None

    logger.info('Compute the estimator of the log normalized constant.')
    log_normalizing_constant = np.log(
        normalizing_constant_estimator(obs, param_mle, model)
    )

    samples_mle = sampling.metropolis_hastings(nsamples, obs, param_mle, model,
                                               burnin, thinning)
    samples_mle_ss = np.array([
        model.sufficient(graph) for graph in samples_mle
    ])
    params = sufficient_statistics = None
    if method == 'snis':
        logger.info('Compute a first set of sufficient statistics for the '
                    'self-normalized importance sampling method.')
        params = np.array([param_mle])
        sufficient_statistics = np.array([samples_mle_ss])

    logger.info('Decompose the variance matrix using Cholesky')
    mu = np.copy(mu_init)
    chol = scipy.linalg.cholesky(var_init, lower=True)
    chol_prime = np.copy(chol)
    np.fill_diagonal(chol_prime, np.log(np.diag(chol)))
    chol_prime = _vech(chol_prime)

    logger.info('Compute some constants used for the algorithm.')
    log_det_var_init = np.log(np.linalg.det(var_init))
    inv_var_init = np.linalg.inv(var_init)
    mu_first_moment_mean = np.zeros_like(mu)
    mu_second_moment_mean = np.zeros_like(mu)
    chol_prime_first_moment_mean = np.zeros_like(chol_prime)
    chol_prime_second_moment_mean = np.zeros_like(chol_prime)
    eps = 1
    evaluation_period = 1000
    t = 0

    logger.info('Compute the initial estimator of the evidence lower bound '
                '(ELBO)')
    s = np.random.multivariate_normal(np.zeros_like(mu),
                                      np.eye(mu.shape[0]))
    param = chol @ s + mu
    old_elbo = elbo_estimator(
        param, chol, s, obs_ss, samples_mle_ss, param_mle,
        log_normalizing_constant, log_det_var_init, inv_var_init,
        mu_init
    )
    elbo_estimations = np.empty((evaluation_period,))
    elbo_estimations[0] = old_elbo
    logger.info('Initial estimator of ELBO: {}'.format(old_elbo))

    logger.info('Start iterating...')
    while eps > tol:
        t += 1
        s = np.random.multivariate_normal(np.zeros_like(mu),
                                          np.eye(mu.shape[0]))
        param = chol @ s + mu

        expected_ss = None
        if method == 'snis':  # SNIS:
            chol_inv = np.linalg.inv(chol)
            dist = scipy.spatial.distance.cdist(params,
                                                param.reshape((1, -1)),
                                                metric='mahalanobis',
                                                VI=chol_inv.T @ chol_inv)
            min_idx = np.argmin(dist)
            closest_param = params[min_idx]
            closest_sufficient_statistics = sufficient_statistics[min_idx]
            weights = np.exp(
                closest_sufficient_statistics @ (param - closest_param)
            )
            normalized_weights = weights / weights.sum()
            ess = 1 / (normalized_weights ** 2).sum()
            if ess < nsamples / 3:  # Have to simulate new samples
                samples = sampling.metropolis_hastings(nsamples, obs, param,
                                                       model, burnin, thinning)
                samples_ss = np.array([
                    model.sufficient(graph) for graph in samples
                ])
                sufficient_statistics = np.concatenate((
                    sufficient_statistics,
                    samples_ss[np.newaxis]
                ))
                params = np.concatenate((
                    params, param[np.newaxis]
                ))
                expected_ss = samples_ss.mean(0)
            else:
                expected_ss = (normalized_weights[:, np.newaxis] *
                               closest_sufficient_statistics).sum()
        elif method == 'montecarlo':  # Monte Carlo Sampling
            samples = sampling.metropolis_hastings(nsamples, obs, param,
                                                   model, burnin, thinning)
            samples_ss = np.array([
                model.sufficient(graph) for graph in samples
            ])
            expected_ss = samples_ss.mean(0)

        # Compute the gradients.
        grad_mu = obs_ss - expected_ss - \
            inv_var_init @ (param - mu_init) + \
            np.linalg.solve(chol.T, s)
        grad_chol = _vech(grad_mu.reshape((-1, 1)) @ s.reshape((1, -1)))
        chol_tilde = np.ones_like(chol)
        np.fill_diagonal(chol_tilde, np.diag(chol))
        d_chol = np.diag(_vech(chol_tilde))
        grad_chol_prime = d_chol @ grad_chol

        # Proceed with a step.
        mu_first_moment_mean = first_moment_decay * mu_first_moment_mean \
            + (1 - first_moment_decay) * grad_mu
        mu_second_moment_mean = second_moment_decay * mu_second_moment_mean \
            + (1 - second_moment_decay) * grad_mu ** 2
        mu_first_moment_estimation = mu_first_moment_mean \
            / (1 - first_moment_decay ** t)
        mu_second_moment_estimation = mu_second_moment_mean \
            / (1 - second_moment_decay ** t)
        chol_prime_first_moment_mean = \
            first_moment_decay * chol_prime_first_moment_mean \
            + (1 - first_moment_decay) * grad_chol_prime
        chol_prime_second_moment_mean = \
            second_moment_decay * chol_prime_second_moment_mean \
            + (1 - second_moment_decay) * grad_chol_prime ** 2
        chol_prime_first_moment_estimation = chol_prime_first_moment_mean \
            / (1 - first_moment_decay ** t)
        chol_prime_second_moment_estimation = chol_prime_second_moment_mean \
            / (1 - second_moment_decay ** t)
        mu = mu + learning_step * mu_first_moment_estimation \
            / (np.sqrt(mu_second_moment_estimation) + 1e-10)
        chol_prime = chol_prime + learning_step \
            * chol_prime_first_moment_estimation \
            / (np.sqrt(chol_prime_second_moment_estimation) + 1e-10)

        chol[np.tril_indices(chol.shape[0])] = np.copy(chol_prime)
        np.fill_diagonal(chol, np.exp(np.diag(chol)))

        elbo_estimations[t % evaluation_period] = elbo_estimator(
            param, chol, s, obs_ss, samples_mle_ss, param_mle,
            log_normalizing_constant, log_det_var_init, inv_var_init,
            mu_init
        )
        if t % evaluation_period == 0:
            new_elbo = elbo_estimations.mean()
            eps = np.abs(new_elbo - old_elbo) / np.abs(old_elbo)
            old_elbo = new_elbo
            logger.info(
                'At iteration: {} ELBO: {:.6f} epsilon: {:.6f}'\
                .format(t, new_elbo, eps)
            )

    return mu, chol @ chol.T
