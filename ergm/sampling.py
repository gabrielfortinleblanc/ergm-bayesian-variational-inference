import logging
import random
from concurrent import futures

import numpy as np


def metropolis_hastings(nsamples, y, param, model, burnin, thinning=1,
                        keep_trace=False):
    """
    Samples using a Metropolis-Hastings algorithm `nsamples` from the
    distribution induced by the parameter `param` and the model `model`.

    Parameters
    ==========
    nsamples : int
        The number of samples to sample.
    y : NetworkX Graph
        The initial graph to start the chain.
    param : Numpy Array
        The array of parameters.
    model : Model
        The model to use for computing the sufficient statistics. The vector
        of sufficient statistics computed by it must has the same size as
        `param`.
    burnin : int
        The number of state to drop as burn-in.
    thinning : int (Optional)
        Add states to the sample at each `thinning` states. By default, the
        thinning factor is 1, meaning that after the burnin every state is
        added to the sample.
    keep_trace : bool
        If it is at True, then every states are collected and returned at the
        end of the algorithm. By default, `keep_trace` is at False.
    Returns
    =======
    A list of `nsamples` NetworkX graphs. If `keep_trace` is at True, then a
    2-tuple is returned with the list of samples as first element and the list
    of every states as second element.
    """
    logger = logging.getLogger(__name__)
    logger.info('The function metropolis_hastings is called.')

    peek = y.copy()
    peek_sufficient = model.sufficient(y)
    samples = list()
    aux_samples = list() if keep_trace else None

    t = 0
    last_t = burnin - 1
    while len(samples) < nsamples:
        # Pick random pair of nodes
        nodes = list(peek.nodes())
        u = random.choice(nodes)
        nodes.remove(u)
        v = random.choice(nodes)
        in_graph = peek.has_edge(u, v)

        # Compute the sufficient statistic of the possible next state
        if in_graph:
            peek.remove_edge(u, v)
        else:
            peek.add_edge(u, v)
        temp_sufficient = model.sufficient(peek)

        # Pass to the next state or stay at the current state
        prob = min(1, np.exp(param.T @ (temp_sufficient - peek_sufficient)))
        unif = random.random()
        if unif < prob:  # Accept the state
            peek_sufficient = temp_sufficient
        else:  # Refuse the state
            if in_graph:
                peek.add_edge(u, v)
            else:
                peek.remove_edge(u, v)

        # Add to the sample if the burn-in is over and if the thinning factor
        # is respected.
        if burnin > 0:
            burnin -= 1
        elif t == last_t + thinning:
            samples.append(peek.copy())
            last_t = t
        t += 1

        if keep_trace:
            aux_samples.append(temp_sufficient)

    return (samples, aux_samples) if keep_trace else samples


def parallel_metropolis_hastings(nsamples, y, param, model, burnin,
                                 nworkers=None):
    """
    Samples using a modified Metropolis-Hastings algorithm `nsamples` from the
    distribution induced by the parameter `param` and the model `model`.
    Instead of creating only one Markov chain and sampling on it, it creates
    `nsamples` chains and for each of those chains, it samples one graph.

    Parameters
    ==========
    nsamples : int
        The number of samples to sample.
    y : NetworkX Graph
        The initial graph to start the chain.
    param : Numpy Array
        The array of parameters.
    model : Model
        The model to use for computing the sufficient statistics. The vector
        of sufficient statistics computed by it must has the same size as
        `param`.
    burnin : int
        The number of state to drop as burn-in.
    nworkers : int (Optional)
        The number of workers to use. By default, it will take all the
        available threads.
    Returns
    =======
    A list of `nsamples` NetworkX graphs.
    """
    logger = logging.getLogger(__name__)
    logger.info('The function parallel_metropolis_hastings is called.')

    future_results = list()
    samples = list()
    with futures.ThreadPoolExecutor(max_workers=nworkers) as executor:
        for _ in range(nsamples):
            future_results.append(
                executor.submit(metropolis_hastings,
                                1, y, param, model, burnin)
            )
        for future in futures.as_completed(future_results):
            samples.append(future.result()[0])
    return samples
