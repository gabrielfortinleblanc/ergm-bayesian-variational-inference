import logging
from concurrent import futures

import numpy as np

from . import sampling


def normalizing_constant_estimator(param, obs, model, nsamples=100,
                                   burnin=1000, thinning=100, ntimes=100):
    """
    Computes an estimator of the normalizing constant by using imporance
    sampling.

    Parameters
    ==========
    param : Numpy Array
        The parameter of the distribution.
    obs : NetworkX Graph
        The observed graph.
    model : Model
        The model used in the analysis.
    nsamples : int
        The number of samples to generate for estimating. By default, it is at
        100.
    burnin : int
        The burnin used when generating graphs. By default, it is at 1000.
    thinning : int
        The thinning factor used when generating graphs. By default, it is at
        100.
    mtimes : int
        The number - 1 of ratios to compute for estimating the normalizing
        constant. By default, it is at 100.
    nworkers : int
        The maximum number of workers to compute this task. If it is at None,
        the maximum number of workers will be used with respect to the
        available resources.
    Returns
    =======
    An estimator of the normalizing constant.
    References
    ==========
    Lampros Bouranis, Nial Friel & Florian Maire (2018) Bayesian Model
    Selection for Exponential Random Graph Models via Adjusted
    Pseudolikelihoods, Journal of Computational and Graphical Statistics, 27:3,
    516-528, DOI: 10.1080/10618600.2018.1448832
    """
    logger = logging.getLogger(__name__)
    logger.info('The function normalizing_constant_estimator has been called.')

    delta_time = 1 / ntimes
    time = np.arange(0, 1, delta_time)
    nratios = time.shape[0] - 1
    ratios = np.empty((nratios,))
    nvertices = obs.number_of_nodes()
    samples = sampling.metropolis_hastings(nsamples, obs, param, model,
                                           burnin, thinning)

    if nworkers == 1:
        for i in range(nratios):
            ratios[i] = _ratio_estimator(sample, model, time, delta_time,
                                         param, nsamples, burnin, thinning)
    else:
        future_results = list()
        with futures.ThreadPoolExecutor(max_workers=nworkers) as executor:
            for i in range(nratios):
                future_results.append(
                    executor.submit(_ratio_estimator,
                                    sample, model, time[i], delta_time, param,
                                    nsamples, burnin, thinning)
                )
            for i, future in enumerate(futures.as_completed(future_results)):
                ratios[i] = future.result()
    return 2 ** (nvertices * (nvertices - 1) / 2) * ratios.prod()


def _ratio_estimator(param, samples_ss, delta_time,
                     nsamples, burnin, thinning):
    """
    This function is used by normalizing_constant_estimator to parralelize the
    task. It returns the estimation of one ratio from the method presented in
    the article of Bouranis, Nial, Friel & Maire.

    Parameters
    ==========
    sample : NetworkX Graph
        The observed graph.
    model : Model
        The model used in the analysis.
    time : float
        The time by which the parameter `param` is multiplied.
    delta_time : float
        The difference between two time steps.
    param : Numpy Array
        The parameter of the distribution.
    nsamples : int
        The number of samples to generate for estimating.
    burnin : int
        The burnin used when generating graphs.
    thinning : int
        The thinning factor used when generating graphs.
    Returns
    =======
    An estimator of a ratio.
    """
    return np.exp(delta_time * samples_ss @ param).mean()
