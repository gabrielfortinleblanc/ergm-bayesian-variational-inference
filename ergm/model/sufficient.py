"""
This modules contains all function to compute sufficient statistics.
"""
import networkx as nx
import numpy as np


def geo_weighted_degree(y, decay):
    """
    Returns the geometrically weighted degree of the input graph.
    This statistics models the degree distribution of the network.

    Parameters
    ==========
    y : NetworkX Graph
        The graph to process.
    decay : Real
        The decay parameter.
    Returns
    =======
    The geometrically weighted degree.
    """
    degrees = np.array([d for _, d in y.degree()])
    uniques, counts = np.unique(degrees, return_counts=True)
    weighted_degrees = (1 - (1 - np.exp(-decay)) ** uniques) * counts
    return np.exp(decay) * weighted_degrees.sum()


def geo_weighted_ew_shared_partners(y, decay):
    """
    Returns the geometrically weighted edgewise shared partners of the input
    graph. This statistics models the degree distribution of the network.

    Parameters
    ==========
    y : NetworkX Graph
        The graph to process.
    decay : Real
        The decay parameter.
    Returns
    =======

    =======
    The geometrically weighted edgewise shared partners.
    """
    adj_matrix = nx.to_numpy_array(y)
    n_common_neighbours = (adj_matrix @ adj_matrix) * adj_matrix
    upper_diag_idx = np.triu_indices(adj_matrix.shape[0], 1)
    uniques, counts = np.unique(n_common_neighbours[upper_diag_idx],
                                return_counts=True)
    weighted_ew_shared_partners = \
        (1 - (1 - np.exp(-decay)) ** uniques) * counts
    return np.exp(decay) * weighted_ew_shared_partners.sum()
