import numpy as np


class Model:

    def __init__(self, sufficients, params):
        """
        Initializes a model with respect to sufficient statistics.

        Parameters
        ==========
        sufficients : List of function
            A list of function to compute the sufficient statistics. Each
            function must take the graph as first parameter.
        params : List of list of parameters
            A list with the same length as `sufficients`. Its elements are list
            of additionnal parameters for the functions in `sufficients`. If
            a specific function a the index `i` does not take any additionnal
            parameters, then `None` of an empty list must be provided at the
            position `i`.
        """
        self._sufficients = sufficients
        self._params = params

    def sufficient(self, y):
        """
        Computes and returns the vector of sufficient statistics.

        Parameters
        ==========
        y : NetworkX Graph
            The graph to process.
        Returns
        =======
        The vector of sufficient statistics.
        """
        sufficient_vector = np.empty((len(self._sufficients),), dtype=float)
        for i, function in enumerate(self._sufficients):
            if self._params[i] is None or len(self._params[i]) == 0:
                sufficient_vector[i] = function(y)
            else:
                sufficient_vector[i] = function(y, *self._params[i])
        return sufficient_vector
