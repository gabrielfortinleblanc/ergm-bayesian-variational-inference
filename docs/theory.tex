\documentclass{article}
\usepackage[utf8]{inputenc}

\usepackage{xcolor}
\usepackage{hyperref}
\hypersetup{
	colorlinks=true,
	linkcolor=blue,
	linkbordercolor={0 0 1}
}

\usepackage{amsmath}
\usepackage{amssymb}

% Functions
\DeclareMathOperator{\KL}{\text{KL}}
\DeclareMathOperator{\ELBO}{\mathcal{L}}
\DeclareMathOperator{\Esp}{\mathbb{E}}
\DeclareMathOperator{\logit}{\text{logit}}
\DeclareMathOperator{\vech}{\text{vech}}
\DeclareMathOperator{\diag}{\text{diag}}

% Sets
\DeclareMathOperator{\R}{\mathbb{R}}

% Environments
\newtheorem{theorem}{Theorem}

\title{Theory}
\author{Gabriel Fortin-Leblanc}
\date{\today}

\begin{document}
	\maketitle
	
	\section{Introduction}
	Graphs with $n$ vertices are represented with a $n \times n$ adjacency matrix $Y$ where the element $Y_{ij}$ equals to 1 if it there is an edge between the vertices $i$ and $j$. The set $\mathcal{Y}$ contains every graph of $n$ vertices. We consider here that graphs are undirected and have no loop which means that $Y_{ij} = Y_{ji}$, for any $i \ne j$ and $Y_{ii} = 0$ for any $i$. For any $y \in \mathcal{Y}$, its likelihood is
	$$
	p(y|\theta) = \frac{\exp\{\theta^T s(y)\}}{z(\theta)},
	$$
	where $\theta \in \R^m$ is the parameter of the distribution, $s(y) \in \R^m$ is a vector of sufficient statistics and
	$$
	z(\theta) = \sum_{y \in \mathcal{Y}} \exp\{\theta^T s(y)\}
	$$
	is a normalizing constant. We can see that it is a family of distributions since the definition of the function $s$ has to be made. The decision of which sufficient statistics to use depends on the application of the model. Summing through every graph is computationally impossible since it exists $2^{\binom{n}{2}}$ graphs of $n$ vertices.
	
	We want to find the posterior distribution of the parameter with respect to the observation. By Bayes' theorem, we have
	$$
	p(\theta|y) = \frac{p(y|\theta) p(\theta)}{p(y)} = \frac{p(y|\theta) p(\theta)}{\int p(y|\theta)p(\theta)d\theta}.
	$$
	We have that the likelihood is intractable and the integral at the denominator is also intractable that is why we describe this problem as \textit{doubly intractable}. To tackle this problem, we will need some tools that we describe in the following sections.
	
	\section{Generating graphs} \label{generating-graphs}
	Snijders proposes a Metropolis-Hastings algorithm to draw pseudo-random graphs in \cite{Snijders2002}. With a starting graph $y^{(0)}$, it creates a Markov chain by adding or removing edges at most once per step. Let $y_{kl}^{(i)}$ be the graph at state $i$ of the chain, but with a flipped edges between the vertices $k$ and $l$, meaning that the edge is removed if it was present in $y^{(i)}$ or added if it wasn't. From a graph $y^{(i)}$, we pass to the graph $y_{kl}^{(i)}$ with the probability
	$$
	\min\left\{1, \exp\left(\theta^T
	(s(y_{kl}^{(i)}) - s(y^{(i)}))\right)\right\}
	$$
	This algorithm surely takes a $\theta$ as parameter, but also an initial graph and a burn-in constant meaning how many state it has to be dropped before accepting graphs as being part of the sample. A thinning factor can be used to reduce the correlation between elements of the sample.
	% TODO: Show that it respect the criterion of a Markov chain.
	% TODO: How to evaluate the sample?
	% TODO: What Snijders proposes when we are stuck in a mode (bit-flipping)?
	
	\section{Pseudolikelihood} \label{pseudolikelihood}
	Strauss and Ikeda provide an estimation of $\theta_{ML}$ in \cite{StraussIkeda1990} the maximum pseudolikelihood estimator. It may be biased but it should not be so far from the true $\theta_{ML}$. They assume that each dyad is independent knowing the other dyads. We have now
	$$
	f_{PL}(y|\theta) = \prod_{1 \le i < j \le n} p(Y_{ij}|Y_{-ij}, \theta),
	$$
	with $Y_{-ij}$ all the values of the adjacency matrix except for $i, j$. With this condition, they define a logistic model
	$$
	\logit\left(p(Y_{ij} = 1|Y_{-ij}, \theta)\right)
	= \log \frac{p(Y_{ij} = 1|Y_{-ij}, \theta)}{p(Y_{ij} = 0|Y_{-ij}, \theta)}
	= \theta^T (s(y_{ij}^+) - s(y_{ij}^-)),
	$$
	where $y_{ij}^+$ is the network $`y`$ but with $Y_{ij} = 1$ and conversely, $y_{ij}^-$ is the network $y$ with $Y_{ij} = 0$. The parameter of this model can be easily found with existing packaged since we use the logistic regression with $Y_{ij}$ as response variables and $s(y_{ij}^+) - s(y_{ij}^-)$ explicative variables.
	% TODO: Explicit the conditions (independances) with calculations.
	
	% TODO: What about Adjusted pseudolikelihood?
	
	\section{Maximum Likelihood Estimation} \label{maximum-likelihood-estimation}
	Since the likelihood is intractable, it is not possible to find the maximum likelihood $\theta_{ML}$ as usual. Instead, we maximize the log ratio of the likelihoods
	\begin{align*}
		LR_{\theta_0}(\theta)
		&= \log \frac{p(y|\theta)}{p(y|\theta_0)} \\
		&= s(y)^T (\theta - \theta_0) - \log \frac{z(\theta)}{z(\theta_0)} \\
		&= s(y)^T (\theta - \theta_0) - \log \mathbb{E}_{y|\theta_0} \left(\exp(s(y)^T(\theta - \theta_0))\right),
	\end{align*}
	at $\theta$ and some initial estimate $\theta_0$ that we want close to the sought value. Using the fact that
	$$
	\frac{z(\theta)}{z(\theta_0)} = \sum_{y \in \mathcal{Y}} \frac{\exp(\theta^Ts(y))p(y|\theta_0)}{\exp(\theta_0^Ts(y))} = \mathbb{E}_{y|\theta_0} \left(\exp(s(y)^T(\theta - \theta_0))\right),
	$$
	we can approximate the log ratio by
	$$
	LR_{\theta_0}(\theta) \approx \phi_{\theta_0}(\theta) =
	s(y)^T (\theta - \theta_0) -
	\log \left(\frac{1}{K}\sum_{k=1}^{K} \exp(s_k(\theta))\right).
	$$
	if we possess $y_1, y_2, \dots, y_K \sim p(\cdot|\theta_0)$. This sample can be obtained by the algorithm presented in Section \ref{generating-graphs}. Remark, that the second member is no longer the log ratio of likelihoods, but a simple approximation, and it isn't guaranteed that has a maximum. We can compute the gradient and Hessian matrix to set conditions for having our optimum. For every $k, l = 1, 2, \dots, K$, let
	$$
	w_k(\theta) = \frac{\exp(s(y_k)^T(\theta - \theta_0))}{\sum_{k=1}^{K} \exp(s(y_k)^T(\theta - \theta_0))}
	$$
	and
	$$
	w_{k, l}(\theta) = \frac{\exp((s(y_k) + s(y_l))^T(\theta - \theta_0))}{\sum_{k=1}^{K} \exp(s(y_k)^T(\theta - \theta_0))}.
	$$
	The gradient is
	$$
	\nabla_{\theta} \phi_{\theta_0} (\theta) = s(y) - \frac{\sum_{k=1}^{K} \exp(s(y_k)^T(\theta - \theta_0))s(y_k)}{\sum_{k=1}^{K} \exp(s(y_k)^T(\theta - \theta_0))} = \sum_{k=1}^{K} w_k(\theta)(s(y) - s(y_k)).
	$$
	We can see that if $s(y)$ is on the edge of the domain of the sufficient statistics, then the optimum is necessarily on the edge, that can lead to undesired model estimation.
	The Hessian matrix is
	$$
	\nabla_{\theta}^2 \phi_{\theta_0} (\theta) = \sum_{k=1}^{K} \sum_{l=1}^{K}
	w_{k, l}(\theta)(s(y) - s(y_k))(s(y_k) - s(y_l))^T.
	$$
	If $s(y)$ is in the convex hull of $s(y_1), s(y_2), \dots, s(y_K)$, we can write
	$$
	s(y) = \sum_{k=1}^{K} \lambda_k s(y_k)
	$$
	with $\lambda_1, \lambda_2, \dots, \lambda_K > 0$ and $\sum_{k=1}^{K} \lambda_k = 1$. We call $\lambda_1, \lambda_2, \dots, \lambda_K$ a convex combination. With it, we have that
	$$
	\nabla_{\theta}^2 \phi_{\theta_0} (\theta) = \sum_{k=1}^{K} \sum_{m=1}^{K}
	\lambda_m(s(y_m) - s(y_k))\sum_{l=1}^{K} w_{k,l}(\theta)
	(s(y_k) - s(y_l))^T.
	$$
	For $k = 1, 2, \dots, K$, let $S_k$ be the matrix composed of
	$$
	\{s(y_1) - s(y_k), s(y_2) - s(y_k), \dots, s(y_K) - s(y_k)\}
	$$
	as columns,
	$$
	\lambda =
	\begin{pmatrix}
		\lambda_1 \\
		\lambda_2 \\
		\vdots \\
		\lambda_K
	\end{pmatrix}
	\quad\text{and}\quad
	\pi_k(\theta) =
	\begin{pmatrix}
		w_{k, 1}(\theta) \\
		w_{k, 2}(\theta) \\
		\vdots \\
		w_{k, K}(\theta)
	\end{pmatrix}.
	$$
	By rewriting the Hessian matrix as
	\begin{align*}
		\nabla_{\theta}^2 \phi_{\theta_0} (\theta)
		&= -\sum_{k=1}^{K} \sum_{m=1}^{K} \lambda_m(s(y_m) - s(y_k))\sum_{l=1}^{K} w_{k,l}(\theta) (s(y_l) - s(y_k))^T \\
		&= -\sum_{k=1}^{K} S_k \lambda (S_k \pi_k(\theta))^T \\
		&= -\sum_{k=1}^{K} S_k \lambda \pi_k(\theta)^T S_k^T,
	\end{align*}
	we can easily show that it is semi-negative definite. Since the matrix $\lambda \pi_k(\theta)^T$ as a rank of 1 (that can be easily seen by developing the matrix) for every $k$, it has only one non-zero eigen value. Because the trace of a matrix is the sum of its eigen values, and by the definition of $\lambda$ and $\pi_k(\theta)$, its only non-zero eigen value is positive, hence $\lambda \pi_k(\theta)^T$ is semi-positive definite. It implies that $S_k \lambda \pi_k(\theta)^T S_k^T$ is also semi-positive definite, and then $\nabla_{\theta}^2 \phi_{\theta_0} (\theta)$ is semi-negative definite.
	
	Hummel, Hunter and Handcock propose a two-phase algorithm in \cite{HummerHunterHandcock2012}. For an observed graph $y$,
	\begin{enumerate}
		\item Set $t$, the number of iteration the pseudo-observation to zero, and $\theta^{(t)} = \theta^{(0)}$ the initial parameter. This last can be the estimation of the maximum pseudolikelihood.
		\item Use the algorithm proposed in Section \ref{generating-graphs} to generate $K$ graphs $y_1, y_2, \dots, y_K$ that follow $p(\cdot|\theta^{(t)})$.
		\item Compute the mean of the sufficient statistics of the generated sample
		$$
		\bar{\xi}_t = \frac{1}{K} \sum_{k=1}^{K} s(y_k).
		$$
		\item Find the greatest $\gamma_t \in (0, 1]$ such that
		$$
		1.05 \times \gamma_t s(y) + (1 - 1.05 \times \gamma_t) \bar{\xi}
		$$
		is in the convex hull of $y_1, y_2, \dots, y_K$. Then, let
		$$
		\hat{\xi}_t = \gamma s(y) + (1 - \gamma) \bar{\xi}
		$$
		be the pseudo-observation.
		\item Find the next parameter $\theta^{(t + 1)}$ such that
		$$
		\theta^{(t + 1)} = \arg \max_\theta \phi_{\theta^{(t)}}(\theta)
		$$
		by replacing $s(y)$ by $\hat{\xi}_t$.
		\item Increment $t$. Return to 2 until two consecutive iteration with a $\gamma^{(t)} = 1$ have been completed. After, a last iteration is done with a greater $K$ to complete the algorithm.
	\end{enumerate}
	
	Computing the function $\phi_{\theta_0}$ and its gradient can be done efficiently, but for the Hessian, we may prefer using an approximation. Numerically speaking, the exponential functions can cause overflow since we apply the exponential function to something that may be far from 0. We must rewrite the equation. Let
	$$
	s_k(\theta) = s(y_k)^T(\theta - \theta_0)
	$$
	for $k = 1, 2, \dots, K$, and
	$$
	s_{\max}(\theta) = \max\{s_1(\theta), s_2(\theta), \dots, s_K(\theta)\}.
	$$
	We now have that
	\begin{align*}
		\phi_{\theta_0}(\theta)
		&= s(y)^T (\theta - \theta_0) - \log \left(\frac{1}{K}\sum_{k=1}^{K} \exp(s_k(\theta))\right) \\
		&= s(y)^T (\theta - \theta_0) - \log \left(\frac{\exp(s_{\max}(\theta))}{K}\sum_{k=1}^{K}
		\frac{\exp(s_k(\theta))}{\exp(s_{\max}(\theta))}\right) \\
		&= s(y)^T (\theta - \theta_0) - s_{\max}(\theta) -
		\log \left(\frac{1}{K}\sum_{k=1}^{K} \exp(s_k(\theta) - s_{\max}(\theta))\right),
	\end{align*}
	and
	\begin{align*}
		\nabla_{\theta} \phi_{\theta_0}(\theta)
		&= \sum_{k=1}^{K} \frac{\exp(s_k(\theta))}{\sum_{l=1}^{K}\exp(s_l(\theta))} (s(y) - s(y_k)) \\
		&= \sum_{k=1}^{K} \frac{\exp(s_k(\theta)) \exp(-s_{\max}(\theta))}{\sum_{l=1}^{K}\exp(s_l(\theta))\exp(-s_{\max}(\theta))} (s(y) - s(y_k)) \\
		&= \sum_{k=1}^{K} \frac{\exp(s_k(\theta) - s_{\max}(\theta))}{\sum_{l=1}^{K}\exp(s_l(\theta) - s_{\max}(\theta))} (s(y) - s(y_k)),
	\end{align*}
	which is less likely to overflow.
	
	% TODO: Hummer and al. propose quick way to approximate MLE by supposing that s(y)^T (theta_0 - theta) is normal.
	
	\section{Bayesian Variational Inference} \label{bayesian-variational-inference}
	In Bayesian Variation Inference, the posterior is approximate using optimization. A parameterized distribution family $q_\lambda$ is set, and among every parameter $\lambda$, the one that minimizes the "distance" with the true posterior is chosen as the approximation.
	
	We set the Gaussian distribution as our tractable density $q_\lambda$ with $\lambda = \{\mu, \Sigma\}$. We will find the set of parameter $(\mu, \Sigma)$ that minimizes the Kullback-Leibler (KL) divergence.
	$$
	\KL[q_\lambda(\theta) || p(\theta|y)] = \int q_\lambda(\theta) \log\frac{q_\lambda(\theta)}{p(\theta|y)} d\theta.
	$$
	We know that the KL divergence is lower bounded by the evidence lower bound (ELBO) $\ELBO$ since the KL divergence is non-negative and since we have that
	\begin{align*}
		\log p(y)
		&= \int q_\lambda(\theta) \log p(y) d\theta \\
		&= \int q_\lambda(\theta) \log \frac{p(\theta, y)}{p(\theta|y)} d\theta \\
		&= \int q_\lambda(\theta) \left(\log\frac{p(\theta, y)}{q_\lambda(\theta)} + \log\frac{q_\lambda(\theta)}{p(\theta|y)}\right) d\theta \\
		&= \underbrace{\int q_\lambda(\theta) \log\frac{p(\theta, y)}{q_\lambda(\theta)} d\theta}_{\ELBO} + \underbrace{\int q_\lambda(\theta) \log\frac{q_\lambda(\theta)}{p(\theta|y)} d\theta}_{\KL}.
	\end{align*}
	Because to compute the KL divergence, we must know the sought distribution, we instead maximize the ELBO.
	
	\subsection{Stochastic Variational Inference}
	Tan and Friel propose a stochastic gradient ascent algorithm in \cite{TanFriel2020} to find the couple $(\mu, \Sigma)$ that maximizes the ELBO.
	
	The matrix $\Sigma$ is symmetric and positive definite, thus maximizing with respect to it is not efficient, that is why we prefer to maximize with respect to $C$, where $\Sigma = CC^T$ is the unique Cholesky decomposition. Also, rather than sampling over $\theta \sim \mathcal{N}(\mu, \Sigma)$, we prefer sampling over $s \sim \mathcal{N}(0, I)$, and then obtain our sought sample by $\theta = Cs + \mu$. % TODO: Explain why.
	We define the vectorizing function $\vech(A)$ that vectorizes the lower triangular part of the square matrix $A$.
	
	The unbiaised estimator of the gradients are
	% TODO: Must explicite the calculations
	$$
	\hat{\nabla}_\mu \ELBO = \nabla_{\theta} \log p(\theta, y) + C^{-T}s
	$$
	and
	$$
	\hat{\nabla}_{\vech(C)} \ELBO = \vech\left(\hat{\nabla}_\mu \ELBO s^T\right),
	$$
	with
	$$
	\nabla_{\theta} \log p(\theta, y) = s(y) - \Esp_{y|\theta}[s(y)] - \Sigma^{-1}(\theta - \mu).
	$$
	Nothing guarantee that the elements on the diagonal of $C$ will remain positive. To overcome this problem, we define $C'$ such that $C'_{ii} = \log(C_{ii})$ and $C'_{ij} = C_{ij}$ for all $i \ne j$, and update $C'$ instead. Let $D_C = \diag(\vech(\tilde{C}))$, where $\tilde{C}$ has the same size of $C$ with the same elements on the diagonal, but with ones elsewhere. The estimator of the gradient with respect to $\vech(C')$ is
	$$
	\hat{\nabla}_{\vech(C')} \ELBO = D_C \hat{\nabla}_{\vech(C)} \ELBO.
	$$
	One problem remains: the computation of $\Esp_{y|\theta}[s(y)]$, and Tan and Friel propose two options:
	\begin{itemize}
		\item Monte Carlo Sampling which is computationally expensive but lead to an ``unbiased" estimator of the gradient;
		\item Self-Normalized Importance Sampling which is way less time consuming, but the result is a biased, but consistent, estimator of the gradient.
	\end{itemize}

	\subsubsection{Monte Carlo Sampling}
	At the iteration $t$, we have $\theta^{(t)}$. Using the algorithm presented at Section \ref{generating-graphs}, we sample $y_1, y_2, \dots, y_K \sim p(\cdot|\theta^{(t)})$ and approximate the expected value by $\frac{1}{K} \sum_{k=1}^{K} s(y_k)$.
	
	\subsection{Self-Normalized Importance Sampling}
	% TODO
	
	
	
	\bibliographystyle{plain}
	\bibliography{biblio}
\end{document}