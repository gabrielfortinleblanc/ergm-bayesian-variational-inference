# Exponential Random Graph Models: Bayesian Variational Inference
This repository is used to reproduced the work presented in the article "Bayesian Variational Inference for Exponential Random Graph Models" by Linda S. L. Tan & Nial Friel to properly understand what has been done. The goal is to approximate the posterior $`p(\theta | y)`$ in the Exponential Random Graph Models (ERGM) context.

## Introduction
Graphs are formidable mathematic structures that easily encode relations among objects. Statisticiens try to modelize random graphs (networks) using parameterized distribution family. As an example, the most simple model is certainly the the Erd&odblac;s-Rényi model introduced by Paul Erd&odblac;s and Alfréd Rényi in "On Random Graphs I". This model is simply a Bernoulli experience on every edges. Thus, the probability of that a graph $`y`$ of $`n`$ vertices and $`m`$ edges is
```math
p(y|\theta) = \binom{{\binom{n}{2}}}{m} \theta^m (1 - \theta)^{\binom{n}{2} - m}
```
where $`\theta`$ is the probability that an edge to be in the graph.

In Bayesian statistics, the parameter is random, and we are more interested in the posterior distribution $`p(\theta | y)`$, which is the distribution of the parameter with respect to the observed graph.

## Theory
All the details about the theory used in this repository can be found in the PDF file [theory](https://gitlab.com/gabrielfortinleblanc/ergm-bayesian-variational-inference/-/blob/main/docs/theory.pdf) in the *docs* directory.

## Getting Started
The repository structure is quite simple.
- The *data* directory contains a sub-directory for each data set.
- The *docs* directory contains multiple files relative to the documentation (figures, details of derivative, etc.).
- The *ergm* directory contains the source code.
- The *notebooks* directory contains multiple notebooks that simulate algorithm implemented in *ergm*.
- The *test* directory contains the tests and has the same hierarchy of *ergm*.

### Environment
To reproduce the Python environment, the user first need [Python 3+](https://www.python.org/downloads/). To be able to manipulate the data, he will also need [Git Large File Support (Git-LFS)](https://git-lfs.github.com/). The following commands create the environment with the same dependancies.
```shell
# Create a new environment
python3 -m venv path/to/new/environment
# Activate the environment
source path/to/new/environment/bin/activate
# Install dependancies
# The file requirements.txt can be found at the root of the repository. 
python -m pip install -r requirements.txt
```
The user can deactivate the environment by simply enter
```shell
deactivate
```

To download the data, the user must explicitly say it so.
```shell
git lfs pull
```

### Execute
The user can execute any notebook by first starting a Jupyter Notebook session.
```shell
jupyter notebook
```

### Tests
All tests in the *test* directory will be executed using the command
```shell
pytest
```
at the root of the repository. A report will then be printed.
